<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->command->call('migrate:fresh');

        $this->call(CourseTableSeeder::class);
        $this->call(StudentTableSeeder::class);
        $this->call(TestTableSeeder::class);
        $this->call(MarkTableSeeder::class);
    }
}
