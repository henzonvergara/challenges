<?php

use Illuminate\Database\Seeder;

class StudentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        #factory(\App\Student::class, 10)->create();
        factory(\App\Student::class, 10)->create()->each(function ($student) {
            //create 5 posts for each user
            factory(\App\Mark::class, 5)->create(['student_id'=>$student->id]);
        });
    }
}
