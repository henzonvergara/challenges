<?php

use Faker\Generator as Faker;

$factory->define(App\Test::class, function (Faker $faker) {
    return [
        'course_id' => factory(\App\Course::class)->create()->id,
        'weight' => $faker->numberBetween(1,100)
    ];
});
