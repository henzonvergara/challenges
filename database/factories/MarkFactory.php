<?php

use Faker\Generator as Faker;

$factory->define(App\Mark::class, function (Faker $faker) {
    return [
        'student_id' => factory(\App\Student::class)->create()->id,
        'test_id' => factory(\App\Test::class)->create()->id,
    ];
});
