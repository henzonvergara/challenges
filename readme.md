# About

This project outputs a list of students with their corresponding courses. Each course will have the name of the teacher to whom they are enrolled to and their final grade as well. Each student will also have their total average.

#Installation

<strong>Note:</strong> XAMPP was used in developing this project
<ol>
    <li>Install XAMPP</li>
    <li>Configure xampp\apache\conf\extra\httpd-vhosts.conf. Add new virtual host record for this project.</li>
    <li>Pull project to your system.</li>
    <li>Create database with <u>dbchallenge</u> as database name</li>
    <li>Migrate and Seed database</li>
    <li>Start XAMPP using XAMPP Control Panel</li>
    <li>Access system in your browser using the following format: <br> http://localhost:[virtual_host_port]</li>
</ol>