<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Test extends Model
{
    public $timestamps = false;

    public function course()
    {
        return $this->hasOne('App\Course', 'id', 'course_id');
    }
}
