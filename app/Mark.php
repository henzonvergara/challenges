<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mark extends Model
{
    public $timestamps = false;

    /*
    public function testx()
    {
        return $this->hasOne('App\Test', 'id', 'test_id');
    }
    */

    public function student()
    {
        return $this->belongsTo('App\Student');
    }

    public function test()
    {
        return $this->belongsTo('App\Test');
    }
}
