<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    public $timestamps = false;

    public function marks()
    {
        return $this->hasMany('App\Mark', 'student_id', 'id');
    }
}
