<?php

namespace App\Http\Controllers;

use App\Student;
use Illuminate\Http\Request;

class ChallengeController extends Controller
{
    public function index()
    {
        $students = Student::limit(10)->get();

        $studentAvgs = [];
        foreach ($students as $student)
        {

            $weightTotal = 0;
            $weightCtr = count($student->marks);
            foreach ($student->marks as $mark)
            {
                $weightTotal = $weightTotal + $mark->test->weight;
            }

            $studAvg = $weightTotal/$weightCtr;
            $studentAvgs[$student->id] = $studAvg;

        }

        return view('challenge.index', compact(
            'students',
            'studentAvgs'
        ));
    }
}
