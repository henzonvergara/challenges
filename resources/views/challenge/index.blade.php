@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">

                @foreach($students as $student)
                    <div class="card mb-3">
                        <div class="card-header">
                            Student ID: {{ $student->id }}
                            <br>
                            Student Name: {{ $student->name }}
                            <br>
                            Total Average: {{ $studentAvgs[$student->id] }}
                        </div>
                        <div class="card-body">
                            <table class="table table-sm">
                                <tbody>
                                @foreach($student->marks as $mark)
                                    <tr>
                                        <td>
                                            Course Name: {{ $mark->test->course->name }}, Course Teacher: {{ $mark->test->course->teacher }}
                                            <br>
                                            Final Grade: {{ $mark->test->weight }}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                @endforeach

            </div>
        </div>
    </div>

@endsection